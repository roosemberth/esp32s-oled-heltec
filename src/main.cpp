/** Copyright (C) 2019 Roosembert Palacios
 *
 * Inspired from the expressif idf BSD socket example
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/socket.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include <string.h>

#include "driver/i2c.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_wifi.h"

#include "nvs_flash.h"
#include "SSD1308.h"

#define WIFI_SSID     "Systech-Aurora\0"
#define WIFI_PASS     "qzp3m4bcj\0"

#define BLINK_GPIO    GPIO_NUM_18
#define NETSTAT_GPIO  GPIO_NUM_25
#define OLED_I2C_SCL  GPIO_NUM_15
#define OLED_I2C_SDA  GPIO_NUM_4
#define OLED_RESET    GPIO_NUM_16
#define UDP_PORT 200

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const static int CONNECTED_BIT = BIT0;

static esp_err_t wifi_event_handler(void *ctx, system_event_t *event);
static unsigned int netstat_led_period = 500;
static SSD1308 *oled;

static EventGroupHandle_t wifi_event_group;

static void wifi_conn_init(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK( esp_event_loop_init(wifi_event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    wifi_config_t wifi_config = {};
    strcpy((char*)wifi_config.sta.ssid, WIFI_SSID);
    strcpy((char*)wifi_config.sta.password, WIFI_PASS);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
}

static void heartbeet_task(void *pvParameter)
{
    gpio_pad_select_gpio(BLINK_GPIO);
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    while(1) {
        gpio_set_level(BLINK_GPIO, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        gpio_set_level(BLINK_GPIO, 1);
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}

static void uart_greet()
{
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
}

static void netstat_blink_task(void *pvParameter)
{
    gpio_pad_select_gpio(NETSTAT_GPIO);
    gpio_set_direction(NETSTAT_GPIO, GPIO_MODE_OUTPUT);
    while(1) {
        gpio_set_level(NETSTAT_GPIO, 0);
        vTaskDelay(netstat_led_period / portTICK_PERIOD_MS);
        gpio_set_level(NETSTAT_GPIO, 1);
        vTaskDelay(netstat_led_period / portTICK_PERIOD_MS);
    }
}

static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        netstat_led_period = 100;
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        netstat_led_period = 1000;
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        netstat_led_period = 100;
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}

static void echo_task(void *p)
{
    const char *TAG = "UDP echo";
    char rx_buffer[128];
    char addr_str[128];

    while (1) {
        struct sockaddr_in destAddr = { };
        destAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        destAddr.sin_family = AF_INET;
        destAddr.sin_port = htons(UDP_PORT);
        inet_ntoa_r(destAddr.sin_addr, addr_str, sizeof(addr_str) - 1);

        int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
        if (sock < 0) {
            ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
            break;
        }

        if (bind(sock, (struct sockaddr *)&destAddr, sizeof(destAddr)) < 0) {
            ESP_LOGE(TAG, "Socket unable to bind: errno %d", errno);
            break;
        }

        while (1) {
            struct sockaddr_in sourceAddr;
            socklen_t socklen = sizeof(sourceAddr);

            int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer) - 1, 0, (struct sockaddr *)&sourceAddr, &socklen);
            if (len < 0) {
                ESP_LOGE(TAG, "recvfrom failed: errno %d", errno);
                break;
            }

            inet_ntoa_r(((struct sockaddr_in *)&sourceAddr)->sin_addr.s_addr, addr_str, sizeof(addr_str) - 1);
            rx_buffer[len] = 0; // Null-terminate whatever we received and treat like a string...
            ESP_LOGI(TAG, "Received %d bytes from %s:", len, addr_str);

            if (sendto(sock, rx_buffer, len, 0, (struct sockaddr *)&sourceAddr, sizeof(sourceAddr)) < 0) {
                ESP_LOGE(TAG, "Error occured during sending: errno %d", errno);
                break;
            }
        }
    }
}

static void lcd_task(void *p)
{
  gpio_pad_select_gpio(OLED_RESET);
  gpio_set_direction(OLED_RESET, GPIO_MODE_OUTPUT);
  gpio_set_level(OLED_RESET, 0);
  vTaskDelay(100 / portTICK_PERIOD_MS);
  gpio_set_level(OLED_RESET, 1);
  while (1) {
    oled->writeString(0, 0, "Hello World !");

    oled->fillDisplay(0xAA);
    oled->setDisplayOff();
    vTaskDelay(500 / portTICK_PERIOD_MS);
    oled->setDisplayOn();

    oled->clearDisplay();
    oled->setDisplayInverse();
    vTaskDelay(500 / portTICK_PERIOD_MS);
    oled->setDisplayNormal();

    oled->writeString(0, 0, "Hello ! :)");
    vTaskDelay(5000 / portTICK_PERIOD_MS);
  }
}

extern "C" {
  void app_main(void);
}

void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    wifi_conn_init();
    uart_greet();

    gpio_pad_select_gpio(OLED_I2C_SCL);
    gpio_set_direction(OLED_I2C_SCL, GPIO_MODE_OUTPUT);
    gpio_pad_select_gpio(OLED_I2C_SDA);
    gpio_set_direction(OLED_I2C_SDA, GPIO_MODE_OUTPUT);
    oled = new SSD1308(I2C_NUM_0, OLED_I2C_SDA, OLED_I2C_SCL, 0x3c);

    xTaskCreate(heartbeet_task, "heartbeet", 2048, NULL, 5, NULL);
    xTaskCreate(netstat_blink_task, "netstat", 2048, NULL, 5, NULL);
    xTaskCreate(echo_task, "echo", 2048, NULL, 5, NULL);
    xTaskCreate(lcd_task, "LCD", 2048, NULL, 5, NULL);
}
