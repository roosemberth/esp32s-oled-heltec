# esp32 OLED heltec sandbox

This repository is the product of a 12-hour hackaton where I discovered platformIO
and the ESP32s. I used an ESP32 OLED board from Heltec.

What working:
  - UDP echo server (see tag)

What is not working yet
  - Using the OLED screen


Notes:
I took mbed driver library and wrote another implementation of the I2C HAL using
esp's idf primitives.
